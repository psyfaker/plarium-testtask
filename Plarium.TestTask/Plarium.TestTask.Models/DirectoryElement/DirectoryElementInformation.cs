﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Plarium.TestTask.Models.DirectoryElement
{
	public class DirectoryElementInformation
	{
		public string FullName { get; set; }

		public string Name { get; set; }

		public DateTime CreationTimeUtc { get; set; }

		public DateTime ModificationTimeUtc { get; set; }

		public DateTime LastAccessTimeUtc { get; set; }

		public FileAttributes Attributes { get; set; }

		public string Owner { get; set; }

		public List<string> Permissions { get; set; } = new List<string>();

		public float Size { get; set; }
	}
}
