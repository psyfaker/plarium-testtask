﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Plarium.TestTask.Models.DirectoryElement
{
	public class FolderInformation : DirectoryElementInformation
	{
		[XmlIgnore]
		public int Depth { get; set; }

		[XmlIgnore]
		public List<FileInformation> FilesInformation { get; set; } =  new List<FileInformation>();

		[XmlIgnore]
		public List<FolderInformation> FoldersInformation { get; set; } = new List<FolderInformation>();
	}
}
