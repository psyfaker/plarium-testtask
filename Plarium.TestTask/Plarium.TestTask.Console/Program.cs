﻿using Plarium.TestTask.Models.DirectoryElement;
using Plarium.TestTask.Services;
using StructureMap;
using System.Threading.Tasks;

namespace Plarium.TestTask.Console
{
	class Program
	{

		private static IContainer _container;
		private static readonly string ScanningDirectoryName = @"C:\TestFodler";


		static void Main(string[] args)
		{
			_container = Container.For<SmServicesRegistry>();

			var directoryInformationGettingCmd = _container.GetInstance<IDirectoryScanningService>();

			Task.Run(() => directoryInformationGettingCmd.ScanDirectory(ScanningDirectoryName).Wait());
			directoryInformationGettingCmd.FolderInformationCreated += OnFolderInformationCreated;

			System.Console.ReadKey();
		}

		private static void OnFolderInformationCreated(object element, FolderInformationCreatedEventArgs args)
		{
			var elementsSerializingService = _container.GetInstance<IElementsSerializingService>();

			Task.Run(() =>
			{
				elementsSerializingService.SerializeFolderInformation(args.CreatedFolderInformation, args.Depth == 0);

				if (args.Depth == 0)
				{
					elementsSerializingService.SaveSerializedData(ScanningDirectoryName);
				}
			});

			Task.Run(() => DisplayElementsInfo(args.CreatedFolderInformation));
		}

		private static void DisplayElementsInfo(FolderInformation folderInformation)
		{
			System.Console.WriteLine($"Folder name: {folderInformation.FullName}, files inside: {folderInformation.FilesInformation.Count}, folders inside: {folderInformation.FoldersInformation}");
		}
	}
}
