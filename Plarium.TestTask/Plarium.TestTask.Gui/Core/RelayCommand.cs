﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace Plarium.TestTask.Gui.Core
{
	public class RelayCommand : ICommand
	{
		private readonly Action<object> _execute;
		private readonly Predicate<object> _canExecute;

		public RelayCommand(Action<object> execute) : this(execute, null) { }

		public RelayCommand(Action<object> execute, Predicate<object> canExecute)
		{
			_execute = execute ?? throw new ArgumentNullException(nameof(execute)); _canExecute = canExecute;
		}

		/// Defines the method that determines whether the command can execute in its current state.
		[DebuggerStepThrough]
		public bool CanExecute(object parameter)
		{
			return _canExecute?.Invoke(parameter) ?? true;
		}

		/// Occurs when changes occur that affect whether or not the command should execute.
		public event EventHandler CanExecuteChanged
		{
			add => CommandManager.RequerySuggested += value;
			remove => CommandManager.RequerySuggested -= value;
		}


		/// Defines the method to be called when the command is invoked.
		public void Execute(object parameter)
		{
			_execute(parameter);
		}
	}
}
