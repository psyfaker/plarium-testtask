﻿using Plarium.TestTask.Services;
using StructureMap;
using System.Windows;

namespace Plarium.TestTask.Gui
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public static IContainer AppContainer;

		public App()
		{
			AppContainer = Container.For<SmServicesRegistry>();
		}
	}
}
