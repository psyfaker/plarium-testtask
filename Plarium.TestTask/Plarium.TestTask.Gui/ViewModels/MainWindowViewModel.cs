﻿using Microsoft.WindowsAPICodePack.Dialogs;
using Plarium.TestTask.Gui.Core;
using Plarium.TestTask.Models.DirectoryElement;
using Plarium.TestTask.Services.WindowServices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Threading;

namespace Plarium.TestTask.Gui.ViewModels
{
	public class MainWindowViewModel : BaseViewModel
	{
		private readonly IMainWindowService _mainWindowService;

		public string DirectoryPath { get; set; }

		private bool _isEnabled;
		public bool IsEnabled
		{
			get => _isEnabled;
			set => SetProperty(ref _isEnabled, value);
		}

		public ObservableCollection<FolderInformation> FoldersInformation { get; set; } = new ObservableCollection<FolderInformation>();

		public MainWindowViewModel()
		{
			_mainWindowService = App.AppContainer.GetInstance<IMainWindowService>();
		}

		public ICommand GetDirectoryInformationCommand { get { return new RelayCommand(param => GetDirectoryInformation()); } }

		public ICommand GetDirectoryNameCommand { get { return new RelayCommand(param => GetDirectoryName()); } }

		// TODO add Cancellation Token to inform user about long operation
		private void GetDirectoryInformation()
		{
			IsEnabled = false;
			DirectoriesInformation.Clear();
			_mainWindowService.FoldersInformation = DirectoriesInformation;

			_mainWindowService.GetDirectoryInformation(DirectoryPath);

			// WPF doesn't allow View\ViewModel modifying outside the UI thread
			_mainWindowService.FolderInformationCreated += (s, e) => Dispatcher.CurrentDispatcher.Invoke((Action)delegate ()
			{
				AddFoldersInfo(e);
			});
			//TODO set IsEnabled = true, when tree will be built
		}

		private void GetDirectoryName()
		{
			var dialog = new CommonOpenFileDialog
			{
				Title = "Open Folder Dialog",
				IsFolderPicker = true
			};

			if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
			{
				DirectoryPath = dialog.FileName;
				IsEnabled = true;
			}
		}

		private void AddFoldersInfo(FolderInformation folderInformation)
		{
			lock (DirectoriesInformation)
			{
				if (DirectoriesInformation.Count != 0 && folderInformation.Depth < DirectoriesInformation[0].Depth)
				{
					DirectoriesInformation.Clear();
				}

				DirectoriesInformation.Add(folderInformation);
			}
		}
	}
}
