﻿using Plarium.TestTask.Models.DirectoryElement;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Plarium.TestTask.Services.DirectoryServices;
using System.Windows.Threading;

namespace Plarium.TestTask.Services.WindowServices
{
	public interface IMainWindowService
	{
		event EventHandler<FolderInformation> FolderInformationCreated;

		ObservableCollection<FolderInformation> FoldersInformation { get; set; }

		void GetDirectoryInformation(string directoryName);
	}

	public class MainWindowService : IMainWindowService
	{
		private readonly IElementsSerializingService _elementsSerializingService;
		private readonly IDirectoryScanningService _directoryScanningService;

		private string DirectoryName { get; set; }

		public event EventHandler<FolderInformation> FolderInformationCreated;

		public ObservableCollection<FolderInformation> FoldersInformation { get; set; }

		public MainWindowService(
			IElementsSerializingService elementsSerializingService,
			IDirectoryScanningService directoryScanningService)
		{
			_elementsSerializingService = elementsSerializingService;
			_directoryScanningService = directoryScanningService;
		}

		public void GetDirectoryInformation(string directoryName)
		{
			DirectoryName = directoryName;

			_directoryScanningService.FolderInformationCreated += OnFolderInformationCreated;

			// Initializing a directory scanning thread
			Task.Run(() => _directoryScanningService.ScanDirectory(DirectoryName)).ConfigureAwait(true);
		}

		private void OnFolderInformationCreated(object element, FolderInformationCreatedEventArgs args)
		{
			// TODO replace Task.Run to execute this operation always on the same thread
			Task.Run(() =>
			{
				_elementsSerializingService.SerializeFolderInformation(args.CreatedFolderInformation, args.Depth == 0);

				if (args.Depth == 0)
				{
					_elementsSerializingService.SaveSerializedData(DirectoryName);
				}
			});

			FolderInformationCreated?.Invoke(this, args.CreatedFolderInformation);
		}
	}
}
