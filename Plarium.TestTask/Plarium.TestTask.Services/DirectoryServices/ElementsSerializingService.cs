﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using Plarium.TestTask.Models.DirectoryElement;

namespace Plarium.TestTask.Services.DirectoryServices
{
	public interface IElementsSerializingService
	{
		void SerializeFolderInformation(FolderInformation folderInformation, bool isRootFolder);

		void SaveSerializedData(string rootDirectoryInfo);
	}

	public class ElementsSerializingService : IElementsSerializingService
	{
		private readonly string _savingPath = @"D:\DirectoriesInfo";

		private static XElement _serializedElementsInfo;

		public ElementsSerializingService()
		{
			_serializedElementsInfo = new XElement("SerializedElementsInfo");
		}

		public void SerializeFolderInformation(FolderInformation folderInformation, bool isRootFolder)
		{
			var directoryElementsInformation = folderInformation
				.FoldersInformation
				.Cast<DirectoryElementInformation>()
				.Concat(folderInformation.FilesInformation)
				.ToList();

			if (isRootFolder)
			{
				directoryElementsInformation.Add(folderInformation);
			}

			lock (_serializedElementsInfo)
			{
				_serializedElementsInfo.Add(Serialize(directoryElementsInformation).Elements());
			}
		}

		public void SaveSerializedData(string rootDirectoryInfo)
		{
			foreach (var invalidFileNameChar in Path.GetInvalidFileNameChars())
			{
				rootDirectoryInfo = rootDirectoryInfo.Replace(invalidFileNameChar, '_');
			}

			if (!Directory.Exists(_savingPath))
			{
				Directory.CreateDirectory(_savingPath);
			}

			lock (rootDirectoryInfo)
			{
				_serializedElementsInfo.Save(_savingPath + @"\" + rootDirectoryInfo + ".xml");
			}
		}

		private XElement Serialize(List<DirectoryElementInformation> directoryElement)
		{
			using (var memoryStream = new MemoryStream())
			{
				using (TextWriter streamWriter = new StreamWriter(memoryStream))
				{
					var xmlSerializer = new XmlSerializer(typeof(List<DirectoryElementInformation>), new[] { typeof(FileInformation), typeof(FolderInformation) });
					xmlSerializer.Serialize(streamWriter, directoryElement);
					return XElement.Parse(Encoding.ASCII.GetString(memoryStream.ToArray()));
				}
			}
		}
	}
}
