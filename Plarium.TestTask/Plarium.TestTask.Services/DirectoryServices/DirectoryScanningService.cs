﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using Plarium.TestTask.Models.DirectoryElement;

namespace Plarium.TestTask.Services.DirectoryServices
{
	public interface IDirectoryScanningService
	{
		event EventHandler<FolderInformationCreatedEventArgs> FolderInformationCreated;

		FolderInformation ScanDirectory(string directoryName, int depth = 0);
	}

	public class DirectoryScanningService : IDirectoryScanningService
	{
		public event EventHandler<FolderInformationCreatedEventArgs> FolderInformationCreated;

		public FolderInformation ScanDirectory(string directoryName, int depth = 0)
		{
			var directoryInformation = new DirectoryInfo(directoryName);
			var folderInformation = CreateFolderInformation(directoryInformation, depth);

			var subDirectoriesInformation = directoryInformation.GetDirectories();
			foreach (var subDirectoryInformation in subDirectoriesInformation)
			{
				var subFolderInformation = ScanDirectory(subDirectoryInformation.FullName, depth + 1);
				folderInformation.FoldersInformation.Add(subFolderInformation);	
			}

			folderInformation.Size += folderInformation.FoldersInformation
				.Sum(subFolderInformation => subFolderInformation.Size);

			FolderInformationCreated?.Invoke(
				this,
				new FolderInformationCreatedEventArgs(folderInformation, depth));

			return folderInformation;
		}

		private FolderInformation CreateFolderInformation(DirectoryInfo directoryInformation, int depth)
		{
			var folderInformation = new FolderInformation()
			{
				FullName = directoryInformation.FullName,
				Name = directoryInformation.Name,
				CreationTimeUtc = directoryInformation.CreationTimeUtc,
				ModificationTimeUtc = directoryInformation.LastWriteTimeUtc,
				LastAccessTimeUtc = directoryInformation.LastAccessTimeUtc,
				Attributes = directoryInformation.Attributes,
				FilesInformation = directoryInformation.EnumerateFiles()
					.Select(CreateFileInformation)
					.ToList(),
				Depth = depth
			};
			folderInformation.Size = folderInformation.FilesInformation.Sum(_ => _.Size);
			SetElementOwnerAndPermissions(folderInformation);

			return folderInformation;
		}

		private FileInformation CreateFileInformation(FileInfo fileInfo)
		{
			var fileInformation = new FileInformation()
			{
				FullName = fileInfo.FullName,
				Name = fileInfo.Name,
				CreationTimeUtc = fileInfo.CreationTimeUtc,
				ModificationTimeUtc = fileInfo.LastWriteTimeUtc,
				LastAccessTimeUtc = fileInfo.LastAccessTimeUtc,
				Attributes = fileInfo.Attributes,
				Size = fileInfo.Length
			};
			SetElementOwnerAndPermissions(fileInformation);

			return fileInformation;
		}

		private void SetElementOwnerAndPermissions(DirectoryElementInformation directoryElementInformation)
		{
			var security = new FileSecurity(directoryElementInformation.FullName,
				AccessControlSections.Owner |
				AccessControlSections.Group |
				AccessControlSections.Access);

			directoryElementInformation.Owner = security.GetOwner(typeof(NTAccount)).ToString();

			var accessRules = security.GetAccessRules(true, true, typeof(NTAccount));
			var currentUser = WindowsIdentity.GetCurrent();
			var principal = new WindowsPrincipal(currentUser);

			var currentUserPermissions = new List<string>();
			foreach (FileSystemAccessRule rule in accessRules)
			{
				var ntAccount = rule.IdentityReference as NTAccount;
				if (ntAccount == null)
				{
					continue;
				}

				if (principal.IsInRole(ntAccount.Value))
				{
					currentUserPermissions.Add(rule.FileSystemRights.ToString());
				}
			}

			directoryElementInformation.Permissions = currentUserPermissions.Distinct().ToList();
		}
	}
}
