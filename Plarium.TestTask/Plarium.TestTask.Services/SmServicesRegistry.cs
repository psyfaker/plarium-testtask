﻿using Plarium.TestTask.Services.DirectoryServices;
using Plarium.TestTask.Services.WindowServices;
using StructureMap;

namespace Plarium.TestTask.Services
{
	public class SmServicesRegistry : Registry
	{
		public SmServicesRegistry()
		{
			For<IElementsSerializingService>().Use<ElementsSerializingService>();
			For<IDirectoryScanningService>().Use<DirectoryScanningService>();
			For<IMainWindowService>().Use<MainWindowService>();
		}
	}
}
