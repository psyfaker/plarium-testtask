﻿using Plarium.TestTask.Models.DirectoryElement;
using System;

namespace Plarium.TestTask.Services
{
	public class FolderInformationCreatedEventArgs : EventArgs
	{
		public FolderInformationCreatedEventArgs(FolderInformation createdFolderInformation, int depth)
		{
			CreatedFolderInformation = createdFolderInformation;
			Depth = depth;
		}

		public FolderInformation CreatedFolderInformation { get; }

		public int Depth { get; }
	}
}
